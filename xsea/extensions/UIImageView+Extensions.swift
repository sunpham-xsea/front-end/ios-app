//
//  UIImageView+Extensions.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/23/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit
extension UIView{
    func rouded(){
        let width = self.frame.width        
        self.layer.masksToBounds = true
        self.layer.cornerRadius = width/2
    }
}
