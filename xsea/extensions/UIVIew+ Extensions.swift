//
//  UIVIew+ Extensions.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/23/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit
extension UIView{
    func makeViewRound(cornerRadius radius: CGFloat){
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
    }
}

extension UIView{
    func setVerticalDirectionGradientColor(from: UIColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1), to: UIColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)){
        let colorLeft =  from.cgColor
        let colorRight = to.cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorLeft, colorRight]
        gradientLayer.startPoint = CGPoint(x: 0,y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0,y: 0.5)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at:0)
    }
}

