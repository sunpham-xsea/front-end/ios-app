//
//  SeaFood.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/25/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation

struct SeaFood: Codable {
    let id: String
    let categoryId: String
    let description: String
    let timeCreated: Int64?
    let timeUpdated: Int64?
    let price: Int
    let makerId: String?
    let ratings: Float
    let imageUrls: [String]?
    let commentIds: [String]?
//    let totalLikes   
}

