//
//  DeliveryType.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/25/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation
import UIKit

struct DeliveryType {
    let name: String
    let code: Int
    let image: UIImage
    let selectedImage: UIImage
    
    public static func allDeleiveries() -> [DeliveryType]{
        return [
            DeliveryType(name: "Free Ship", code: 0, image: #imageLiteral(resourceName: "free-shipping"), selectedImage: #imageLiteral(resourceName: "free_delivery_selected")),
            DeliveryType(name: "Grab", code: 1, image: #imageLiteral(resourceName: "grab"), selectedImage: #imageLiteral(resourceName: "grab")),
            DeliveryType(name: "Now", code: 2, image: #imageLiteral(resourceName: "now"), selectedImage: #imageLiteral(resourceName: "now")),
            DeliveryType(name: "Zalo pay", code: 9, image: #imageLiteral(resourceName: "Icon-app_blue-bg"), selectedImage: #imageLiteral(resourceName: "Icon-app_blue-bg")),
            DeliveryType(name: "Momo", code: 9, image: #imageLiteral(resourceName: "mono"), selectedImage: #imageLiteral(resourceName: "mono")),
            DeliveryType(name: "Cash", code: 9, image: #imageLiteral(resourceName: "cash"), selectedImage: #imageLiteral(resourceName: "cash"))
            
        ]
    }
}
