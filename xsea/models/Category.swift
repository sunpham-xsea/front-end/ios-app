//
//  Category.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/23/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation

struct Category: Codable {
    let id: String
    let name: String
    let code: String
}
