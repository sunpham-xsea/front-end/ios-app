//
//  AddSeaFoodRequest.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/25/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation
struct AddSeaFoodRequest : Codable{
    let categoryId: String
    let name: String
    let description: String
    let price: Int
    let imageUrls: [String]
}
