//
//  Product.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/31/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation

struct Product: Codable{
    let name: String!
    let id: String!
    let price: Int!
    let code: String!
}
