//
//  CustomError.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/25/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

enum XSeaError: Error {
    case UIError(XSeaErrorCode)
    case APIError(_ error: APIErrorInfo)
    case api_error
}

enum XSeaErrorCode: String {
    case IllegalSeaFoodName = "Sea food name required"
    case IllegalSeaFoodCategory = "Category of sea food required"
    case IllegalSeaFoodPrice = "Sea food price required"
    case IllegalSeaFoodDescription  = "Sea food description required"
    case IllegalSeaFoodAdress = "Sea food address required"
    case UnknowError = "Unknow error"
}

enum APIEror: String {
    case internal_error = "internal_eror"
}

enum APIErrorInfo : String {
    case internal_error = "1345"
}
