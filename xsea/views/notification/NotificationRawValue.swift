//
//  NotificationRawValue.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/25/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation
import NotificationCenter

class NotificationCustomName{
    public static let AddSeaFoodSucceessFromPostSeaFoodCell =  Notification.Name("add_sea_food_success_from_post_sea_food_cell")
    public static let SendNewSeaFoodToPostedSeaFood = Notification.Name("send_new_sea_food_to_posted_sea_food")
}
