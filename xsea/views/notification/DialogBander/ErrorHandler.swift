//
//  ErrorHandler.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/25/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class ErrorHandler: NSObject {
    
    private func showNotification(title: String, message: String, style:
        BannerStyle = BannerStyle.danger){
        let banner = GrowingNotificationBanner(title: title, subtitle: message, style: style)
        
        DispatchQueue.main.async {
            banner.show()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.65) {
                banner.dismiss()
            }
        }
        
    }
    
    public func showUIError(_ code: XSeaErrorCode){
        showNotification(title: "Notification", message: code.rawValue, style: .warning)
    }
}
