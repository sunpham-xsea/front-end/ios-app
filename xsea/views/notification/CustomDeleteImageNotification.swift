//
//  CustomDeleteImageNotification.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/24/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class CustomDeleteImageNotification: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CustomPhotoPagerCell{
    private func setupViews(){
        self.backgroundColor = UIColor.red
    }
}
