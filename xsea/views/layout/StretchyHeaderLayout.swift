//
//  StretchyHeaderLayout.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class StretchyFlowLayout: UICollectionViewFlowLayout {
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let layoutAttributes = super.layoutAttributesForElements(in: rect)
        layoutAttributes?.forEach({ (attribute) in
            if attribute.representedElementKind == UICollectionView.elementKindSectionHeader{
                guard let _collectionView = self.collectionView else{
                    return
                }
                let width = _collectionView.frame.width
                let contentOffsetY = _collectionView.contentOffset.y
                let height = attribute.frame.height - contentOffsetY
                
                if contentOffsetY > 0 {
                    return
                }
                
                attribute.frame = CGRect(x: 0, y: contentOffsetY, width: width, height: height)
            }
        })
        return layoutAttributes
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
