//
//  DeliveryCell.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/25/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class DeliveryCell: UICollectionViewCell {
    
    public static let identifier = "DeliveryCell"
    public static let xibName = "DeliveryCell"
    override var isSelected: Bool{
        didSet{
            if self.isSelected{
                self.backgroundColor = #colorLiteral(red: 0.8392156863, green: 0, blue: 0, alpha: 1)
                self.layer.borderWidth = 0
                self.imageView.image = datasource?.selectedImage
                self.nameLabel.textColor = UIColor.white
            } else {
                self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                self.layer.borderWidth = 1
                self.imageView.image = datasource?.image
                self.nameLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var datasource: DeliveryType? = nil {
        didSet{
            if let data = self.datasource {
                imageView.image = data.image
                nameLabel.text = data.name
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupViews()
    }
}

extension DeliveryCell{
    private func setupViews(){
        self.backgroundColor = UIColor.green
        setupLayers()
    }
    
    private func setupLayers(){
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 16
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1
        self.backgroundColor = UIColor.white
    }
}
