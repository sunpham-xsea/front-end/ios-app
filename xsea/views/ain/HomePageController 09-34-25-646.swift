//
//  HomePageController.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit
import Alamofire

class HomePageController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    fileprivate let COLLECTION_CELL_PADDING: CGFloat = 2
    @IBOutlet weak var homePageCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
}

extension HomePageController{
    private func setupViews(){
        setupCollectionView()
    }
}

extension HomePageController{
    private func setupCollectionView(){
        
        self.homePageCollectionView.dataSource = self
        self.homePageCollectionView.delegate = self
        
        homePageCollectionView.collectionViewLayout = StretchyFlowLayout()
        
        homePageCollectionView.contentInsetAdjustmentBehavior = .never
        //
        homePageCollectionView.contentInsetAdjustmentBehavior = .never
        self.homePageCollectionView.backgroundColor = .white
        self.homePageCollectionView.register(UINib(nibName: SeaFoodComponent.xibName, bundle: nil), forCellWithReuseIdentifier: SeaFoodComponent.identifier)
        
        self.homePageCollectionView.register(UINib(nibName: TopRatingsComponent.xibName, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: TopRatingsComponent.indentifier)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SeaFoodComponent.identifier, for: indexPath) as! SeaFoodComponent
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: TopRatingsComponent.indentifier, for: indexPath) as! TopRatingsComponent
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 340)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: homePageCollectionView.frame.width - 2 * COLLECTION_CELL_PADDING, height: homePageCollectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: COLLECTION_CELL_PADDING,
                            left: COLLECTION_CELL_PADDING,
                            bottom: COLLECTION_CELL_PADDING,
                            right: COLLECTION_CELL_PADDING)
    }
    
    
}
