//
//  SeaFoodComponent.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class SeaFoodComponent: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    public static let identifier = "SeaFoodComponentId"
    public static let xibName = "SeaFoodComponent"
    
    let photos = Photo.allPhotos()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupViews()
    }
    
    private func setupViews(){
        self.backgroundColor = UIColor.white
        setupCollectioNViews()
        if let layout = collectionView?.collectionViewLayout as? SeaFoodLayout {
            layout.delegate = self
        }
    }
}

extension SeaFoodComponent: UICollectionViewDelegateFlowLayout{
    private func setupCollectioNViews(){
        collectionView.dataSource = self
        collectionView.delegate = self
        
        
        collectionView.register(UINib(nibName: SeaFoodCell.xibName, bundle: nil), forCellWithReuseIdentifier: SeaFoodCell.identifier)
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 4, bottom: 10, right: 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SeaFoodCell.identifier, for: indexPath) as! SeaFoodCell
        cell.seaFoodImg.image = photos[indexPath.item].image
    
        return cell
    }
}

extension SeaFoodComponent : SeaFoodLayoutDelegate{
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        return photos[indexPath.item].image.size.height
    }
}

