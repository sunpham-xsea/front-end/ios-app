//
//  LoaderSettings.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/28/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//



import UIKit
import Lottie


class LoaderSettings: NSObject {
    
    private let animationView: AnimationView = {
        let animationView = AnimationView()
        animationView.backgroundColor = UIColor.clear
        animationView.animation = Animation.named("loader")
        animationView.loopMode = LottieLoopMode.loop
        animationView.contentMode = UIView.ContentMode.scaleAspectFit
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.backgroundColor = UIColor.clear
        return animationView
    }()
    
//    private let loadingTextView: AnimationView = {
//        let animationView = AnimationView()
//        animationView.backgroundColor = UIColor.clear
//        animationView.animation = Animation.named("loadingtext")
//        animationView.loopMode = LottieLoopMode.loop
//        animationView.contentMode = UIView.ContentMode.scaleAspectFit
//        animationView.translatesAutoresizingMaskIntoConstraints = false
//        return animationView
//    }()
    
    var isLunching: Bool? {
        didSet {
            if isLunching == true {
                showSettings()
            } else {
                handlerDismiss()
            }
        }
    }
    
    let blackView = UIView()
    
    
    override init() {
        super.init()
        //TO DO
        self.setupViews()
    }
}

extension LoaderSettings{
    
    //MARKS: this function for setup constraint layouts for loader animation view type : Lotie
    private func setupViews(){
        self.blackView.addSubview(animationView)
        
        self.animationView.centerXAnchor.constraint(equalTo: self.blackView.centerXAnchor).isActive = true
        self.animationView.centerYAnchor.constraint(equalTo: self.blackView.centerYAnchor).isActive = true
        self.animationView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        self.animationView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
//        self.blackView.addSubview(loadingTextView)
//        self.loadingTextView.topAnchor.constraint(equalTo: self.animationView.topAnchor, constant: 8).isActive = true
//        self.loadingTextView.heightAnchor.constraint(equalToConstant: 100).isActive = true
//        self.loadingTextView.widthAnchor.constraint(equalToConstant: 100).isActive = true
//        self.loadingTextView.centerXAnchor.constraint(equalTo: self.animationView.centerXAnchor).isActive = true
    }
}

extension LoaderSettings {
    public func showSettings() {
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor.black
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            //blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlerDismiss)))
            
            window.addSubview(blackView)
            
            let height = CGFloat(241)
//            let y = window.frame.height / 2 - height / 2
            
            animationView.frame = CGRect(x: CGFloat(20), y: window.frame.height, width: window.frame.width - CGFloat(40), height: height)
            blackView.frame = window.frame
            blackView.alpha = 0
            
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           usingSpringWithDamping: 1,
                           initialSpringVelocity: 1,
                           options: .curveEaseOut,
                           animations: {
                            self.blackView.alpha = 1
                            self.animationView.play()
//                           self.loadingTextView.play()
                            // self.setupViews()
                            //                            self.animationView.frame = CGRect(x: CGFloat(20), y: y, width: window.frame.width - CGFloat(40), height: height)
                            //                            self.switchReportTypePopup.frame = CGRect(x: CGFloat(20), y: y, width: window.frame.width - CGFloat(40), height: height)
            }, completion: nil)
        }
    }
    
    
    @objc func handlerDismiss() {
        self.leaveLoaderView()
    }
    
    private func leaveLoaderView(){
        if let _ = UIApplication.shared.keyWindow {
            UIView.animate(withDuration: 0.5) {
                self.blackView.alpha = 0
                self.animationView.stop()
//                self.loadingTextView.stop()
                
            }
        }
    }
    
}
