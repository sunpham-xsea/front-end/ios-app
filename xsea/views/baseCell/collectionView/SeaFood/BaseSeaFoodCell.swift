//
//  BaseSeaFoodCell.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/28/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class BaseSeaFoodCell: UICollectionViewCell {
    
    public static let identifer  = "BaseSeaFoodCellId"
    public static let xibName = "BaseSeaFoodCell"
    
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }
}

extension BaseSeaFoodCell{
    private func setupViews(){
        self.makeShadows()
    }
    
    private func makeShadows(){
        containerView.backgroundColor = UIColor.white
        containerView.layer.cornerRadius = 8
        containerView.layer.shadowColor = UIColor.darkGray.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerView.layer.shadowRadius = 2
        containerView.layer.shadowOpacity = 0.55
        containerView.translatesAutoresizingMaskIntoConstraints = false
    }
}
