//
//  PostSeaFoodController.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/23/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class PostSeaFoodController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private var selectedImage: UIImage? = nil {
        didSet{
            if let image = self.selectedImage {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "post_selected_photo_lib"), object: self, userInfo: ["image": image])
            }
        }
    }
    
    @IBOutlet weak var deliveryInfoCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotificationListening()
        setupViews()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
      
        

    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleTableViewCell", for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1600
    }
    
}


extension PostSeaFoodController{
    private func setupNotificationListening(){
        NotificationCenter.default.rx.notification(Notification.Name(rawValue: "show_image_picker")).takeUntil(self.rx.deallocated)
            .subscribe(onNext:{
                _ in
                self.showImagePikceViewController()
            })
        
        NotificationCenter.default.rx.notification(NotificationCustomName.AddSeaFoodSucceessFromPostSeaFoodCell).takeUntil(self.rx.deallocated).subscribe { (notification) in
            self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    private func showImagePikceViewController(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        //        imagePicker.allowsEditing = true
        //        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePicker, animated: true) {
            print("show picker controller ok")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            self.selectedImage = selectedImage
            // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "picked_image_view_from_post_sea_food"), object: nil, userInfo: ["image": selectedImage])
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
extension PostSeaFoodController{
    private func setupViews(){
        
        self.navigationController?.navigationBar.isHidden = false
        setupTableView()
    }
}

extension PostSeaFoodController{
    private func setupTableView(){
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        
    }
}

