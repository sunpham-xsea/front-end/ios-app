//
//  PostSeaFoodCell.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/23/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FSPagerView
import iOSDropDown

class PostSeaFoodCell: UITableViewCell {
    
    private let categoryService = Injectors.get(CategoryService.self)
    private let errorHandler = Injectors.get(ErrorHandler.self)
    private let seaFoodservice =  Injectors.get(SeaFoodService.self)
    
    private var categories: [Category] = []{
        didSet{
            if !self.categories.isEmpty{
                self.setupCategoryTextFieldData()
            }
        }
    }
    
    private let bag = DisposeBag()
    private let images = Variable<[UIImage]>([])
    private let deliveries = DeliveryType.allDeleiveries()
    
    @IBOutlet weak var completedButton: UIButton!
    @IBOutlet weak var deliveryInfoCollectionView: UICollectionView!
    @IBOutlet weak var uploadedPhotosPagerView: FSPagerView!
    @IBOutlet weak var uploadImageButton: UIButton!
    
    @IBOutlet weak var foodNameField: UITextField!
    @IBOutlet weak var categoryField: DropDown!
    
    @IBOutlet weak var seaFoodPriceField: UITextField!
    @IBOutlet weak var seaFoodAdressField: UITextField!
    @IBOutlet weak var seaFoodDescriptionField: UITextField!
    
    @IBAction func uploadImageButton_TouchUpInside(_ sender: Any) {
        self.uploadImageButton.shake()
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.showImagePicker()
        }
    }
    
    @IBAction func completedButton_TouchUpInside(_ sender: Any) {
        self.completedButton.flash()
        do{
            let request = try getSeaFoodInfos()
            print("name request params : \(request.name)")
            self.seaFoodservice.createSeaFood(request: request).done { (seaFood) in
                NotificationCenter.default.post(name: NotificationCustomName.AddSeaFoodSucceessFromPostSeaFoodCell, object: self, userInfo: ["sea_food_obj": seaFood])
            }
            .catch { (error) in
                    print(error)
            }
            
        } catch{
            
        }
        
    }
  
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        fetchData()
        setupViews()
        setupTextFieldDelegates()
        images.asObservable().subscribe(onNext: { [weak self] photos in
            self?.uploadedPhotosPagerView.reloadData()
        })
            .disposed(by: bag)
        getImageFromImagePicker()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension PostSeaFoodCell{
    private func setupViews(){
        self.setupUploadImageButtonViews()
        setupPagerView()
        setupDeliveryCollectionView()
        setupCompletedButtonViews()
    }
}

extension PostSeaFoodCell{
    private func setupTextFieldDelegates(){
        self.foodNameField.delegate = self
        self.categoryField.delegate = self
        self.seaFoodAdressField.delegate = self
        self.seaFoodPriceField.delegate = self
        self.seaFoodDescriptionField.delegate = self
    }
}

extension PostSeaFoodCell{
    private func setupCompletedButtonViews(){
        self.completedButton.layer.masksToBounds = true
        self.completedButton.layer.cornerRadius = 8
    }
}
extension PostSeaFoodCell{
    private func setupUploadImageButtonViews(){
        uploadImageButton.makeNoneBackgroundButton(_borderColor: #colorLiteral(red: 0.8156862855, green: 0.2039215714, blue: 0.2235294133, alpha: 1), _backgroundColor: UIColor.clear, _borderWidth: 0.5, _cornerRadius: 8)
    }
    
    private func showImagePicker(){
        //        print("show Image Picker view function running........")
        //        images.value.append(#imageLiteral(resourceName: "ratings_food_default"))
        NotificationCenter.default.post(name: Notification.Name(rawValue: "show_image_picker"), object: self)
        
    }
    
    //    picked_image_view_from_post_sea_food
    
    private func getImageFromImagePicker(){
        NotificationCenter.default.rx.notification(Notification.Name("post_selected_photo_lib")).takeUntil(self.rx.deallocated).subscribe { (notification) in
            if let selectedImage = notification.element?.userInfo?["image"] as? UIImage{
                self.images.value.append(selectedImage)
            }
        }
        
    }
}

extension PostSeaFoodCell: FSPagerViewDataSource, FSPagerViewDelegate{
    
    private func setupPagerView(){
        uploadedPhotosPagerView.delegate = self
        uploadedPhotosPagerView.dataSource = self
        uploadedPhotosPagerView.transformer = FSPagerViewTransformer(type: .overlap)
        let width: CGFloat = uploadedPhotosPagerView.frame.width - 56
        let height: CGFloat = uploadedPhotosPagerView.frame.height
        uploadedPhotosPagerView.itemSize = CGSize(width: width, height: height)
        
        // uploadedPhotosPagerView.backgroundView?.addSubview(UIImageView(image: #imageLiteral(resourceName: "04")))
        uploadedPhotosPagerView.register(CustomPhotoPagerCell.self, forCellWithReuseIdentifier: CustomPhotoPagerCell.identifier)
    }
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return  images.value.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: CustomPhotoPagerCell.identifier, at: index) as! CustomPhotoPagerCell
        cell.imageView?.image = images.value[index]
        return cell
    }
    
}

extension PostSeaFoodCell: UICollectionViewDataSource, UICollectionViewDelegate{
    private func setupDeliveryCollectionView(){
        self.deliveryInfoCollectionView.delegate = self
        self.deliveryInfoCollectionView.dataSource = self
        self.deliveryInfoCollectionView.allowsMultipleSelection = true
        
        self.deliveryInfoCollectionView.register(UINib(nibName: DeliveryCell.xibName, bundle: nil), forCellWithReuseIdentifier: DeliveryCell.identifier)
        
        if let layout = deliveryInfoCollectionView?.collectionViewLayout as? SeaFoodLayout {
            layout.delegate = self
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deliveries.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DeliveryCell.identifier, for: indexPath) as! DeliveryCell
        cell.datasource = deliveries[indexPath.item]
        return cell
    }
    
}

extension PostSeaFoodCell: SeaFoodLayoutDelegate{
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension PostSeaFoodCell{
    
    private func getSeaFoodInfos() throws -> AddSeaFoodRequest{
        do{
            let name = try getName()
            let categoryId = try getCategoryId()
            //let address = try getSeaFoodAddress()
            let price = try getPrice()
            let description = try getDescription()
            
            
            let request = AddSeaFoodRequest(categoryId: categoryId, name: name, description: description, price: price, imageUrls: [])
            return request
        } catch XSeaError.UIError(let code){
            errorHandler.showUIError(code)
        } catch{
            errorHandler.showUIError(XSeaErrorCode.UnknowError)
        }
        
        throw XSeaError.UIError(XSeaErrorCode.UnknowError)
    }
}

extension PostSeaFoodCell{
    private func getName() throws ->  String {
        if let name = foodNameField.text{
            if !name.isEmpty{
                return name
            }
        }
        throw XSeaError.UIError(XSeaErrorCode.IllegalSeaFoodName)
    }
    
    private func getCategoryId() throws -> String{
        
        if let selectedIndex = categoryField.selectedIndex{
            return self.categories[selectedIndex].id
        }
        throw XSeaError.UIError(XSeaErrorCode.IllegalSeaFoodCategory)
    }
    
    private func getPrice() throws -> Int{
        if let priceString = seaFoodPriceField.text{
            if !priceString.isEmpty {
                return Int(priceString)!
            }
        }
        
        throw XSeaError.UIError(XSeaErrorCode.IllegalSeaFoodPrice)
    }
    
    private func getDescription() throws -> String{
        if let description = seaFoodDescriptionField.text{
            if !description.isEmpty {
                return description
            }
        }
        
        throw XSeaError.UIError(XSeaErrorCode.IllegalSeaFoodDescription)
    }
    
    private func getSeaFoodAddress() throws -> String {
        if let address = seaFoodAdressField.text {
            if !address.isEmpty {
                return address
            }
        }
        
        throw XSeaError.UIError(XSeaErrorCode.IllegalSeaFoodAdress)
    }
}

extension PostSeaFoodCell{
    private func setupCategoryTextFieldData(){
        self.categoryField.optionArray = categories.map({ (category) -> String in
            category.name
        })
        self.categoryField.text = categories[0].name
        self.categoryField.selectedIndex = 0
    }
}

extension PostSeaFoodCell{
    private func fetchData(){
        self.categoryService.getCategories()
            .done {
                (categories) in
                self.categories = categories

        }.catch { (error) in
                
            }.finally {
        }
    }
}

extension PostSeaFoodCell: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }

        return true
    }
    
}



