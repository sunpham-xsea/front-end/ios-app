//
//  CustomPhotoPagerCell.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/24/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit
import FSPagerView

class CustomPhotoPagerCell: FSPagerViewCell {
    
    public static let identifier = "CustomPhotoPagerCellId"
    
    private let deleteButton: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor.clear
        btn.setImage(#imageLiteral(resourceName: "delete_image_icon"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}

extension CustomPhotoPagerCell{
    private func setupViews(){
        self.backgroundColor = UIColor.clear
        setupDeleteButton()
    }
}

extension CustomPhotoPagerCell{
    private func setupDeleteButton(){
        self.addSubview(deleteButton)
        deleteButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        deleteButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        deleteButton.widthAnchor.constraint(equalToConstant: 32).isActive = true
        deleteButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        self.deleteButton.addTarget(self, action: #selector(onDeleteImage), for: .touchUpInside)
    }
    
    @objc func onDeleteImage(){
        deleteButton.shake()
        //get index of image delege
        print("delete image at index")
    }
}
