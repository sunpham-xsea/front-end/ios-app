//
//  CategoryDetailRouter.swift
//  xsea
//
//  Created Phạm Anh Tuấn on 8/31/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class CategoryDetailRouter: CategoryDetailWireframeProtocol {

    weak var viewController: UIViewController?

    static func createModule(with category: Category, navColor: UIColor) -> UIViewController {
        // Change to get view from storyboard if not using progammatic UI
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: "CategoryDetailViewController") as! CategoryDetailViewController
        
        
        let interactor = CategoryDetailInteractor()
        let router = CategoryDetailRouter()
        let presenter = CategoryDetailPresenter(interface: view, interactor: interactor, router: router)
        presenter.category = category
        presenter.navigationBarColor = navColor

        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view

        return view
    }
}
