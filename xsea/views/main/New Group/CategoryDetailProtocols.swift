//
//  CategoryDetailProtocols.swift
//  xsea
//
//  Created Phạm Anh Tuấn on 8/31/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import Foundation
import UIKit

//MARK: Wireframe -
protocol CategoryDetailWireframeProtocol: class {

}
//MARK: Presenter -
protocol CategoryDetailPresenterProtocol: class {

    var category: Category! { get set }
    var products: [Product]? {get set}
    var navigationBarColor: UIColor? {get set}
    
    var interactor: CategoryDetailInteractorInputProtocol? { get set }

    func getProductsOfCategory()
}

//MARK: Interactor -
protocol CategoryDetailInteractorOutputProtocol: class {

    /* Interactor -> Presenter */
    func productsOfCategoryDidFetch(with products: [Product])
}

protocol CategoryDetailInteractorInputProtocol: class {

    var presenter: CategoryDetailInteractorOutputProtocol?  { get set }

    /* Presenter -> Interactor */
    func getProductsOfCategory()
}

//MARK: View -
protocol CategoryDetailViewProtocol: class {

    var presenter: CategoryDetailPresenterProtocol?  { get set }

    /* Presenter -> ViewController */
    func showCategoryDetails()
    
}
