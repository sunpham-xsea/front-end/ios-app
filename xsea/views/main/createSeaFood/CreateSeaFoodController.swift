//
//  CreateSeaFoodController.swift
//
//
//  Created by Phạm Anh Tuấn on 8/28/19.
//

import UIKit
import RxSwift
import RxCocoa
import FSPagerView
import iOSDropDown
import GoogleMaps

class CreateSeaFoodController: UIViewController {
    
    
    private let categoryService = Injectors.get(CategoryService.self)
    private let errorHandler = Injectors.get(ErrorHandler.self)
    private let seaFoodservice =  Injectors.get(SeaFoodService.self)
    private let loaderViews = LoaderSettings()
    
    private var categories: [Category] = []{
        didSet{
            if !self.categories.isEmpty{
                self.setupCategoryTextFieldData()
            }
        }
    }
    
    private let bag = DisposeBag()
    private let images = Variable<[UIImage]>([])
    private let deliveries = DeliveryType.allDeleiveries()
    
    @IBOutlet weak var completedButton: UIButton!
    @IBOutlet weak var deliveryInfoCollectionView: UICollectionView!
    @IBOutlet weak var uploadedPhotosPagerView: FSPagerView!
    @IBOutlet weak var uploadImageButton: UIButton!
    
    @IBOutlet weak var foodNameField: UITextField!//MARK : IS
    @IBOutlet weak var categoryField: DropDown!
    
    @IBOutlet weak var seaFoodPriceField: UITextField!
    @IBOutlet weak var seaFoodAdressField: UITextField!
    @IBOutlet weak var seaFoodDescriptionField: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBAction func uploadImageButton_TouchUpInside(_ sender: Any) {
        self.uploadImageButton.shake()
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.showImagePicker()
        }
        
    }
    
    @IBAction func completedButton_TouchUpInside(_ sender: Any) {
        self.completedButton.pulsate()
        do{
            let request = try getSeaFoodInfos()
            DispatchQueue.main.async {
                self.loaderViews.isLunching = true
            }
            self.seaFoodservice.createSeaFood(request: request).done { (seaFood) in
                NotificationCenter.default.post(name: NotificationCustomName.AddSeaFoodSucceessFromPostSeaFoodCell, object: self, userInfo: ["sea_food_obj": seaFood])
                }
                .catch { (error) in
                    print(error)
                
                }.finally {
                    DispatchQueue.main.async {
                        self.loaderViews.isLunching = false
                        self.navigationController?.popViewController(animated: true)
                    }
            }
        } catch{
            
        }
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        setupViews()
        setupTextFieldDelegates()
        images.asObservable().subscribe(onNext: { [weak self] photos in
            self?.uploadedPhotosPagerView.reloadData()
        })
            .disposed(by: bag)
        getImageFromImagePicker()
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension CreateSeaFoodController{
    private func setupViews(){
        self.navigationController?.navigationBar.isHidden = false
        self.setupUploadImageButtonViews()
        setupPagerView()
        setupDeliveryCollectionView()
        setupCompletedButtonViews()
        setupMapViews()
    }
}


extension CreateSeaFoodController{
    private func setupMapViews(){
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView
    }
}
extension CreateSeaFoodController: UICollectionViewDataSource, UICollectionViewDelegate{
    private func setupDeliveryCollectionView(){
        self.deliveryInfoCollectionView.delegate = self
        self.deliveryInfoCollectionView.dataSource = self
        self.deliveryInfoCollectionView.allowsMultipleSelection = true
        
        self.deliveryInfoCollectionView.register(UINib(nibName: DeliveryCell.xibName, bundle: nil), forCellWithReuseIdentifier: DeliveryCell.identifier)
        
        if let layout = deliveryInfoCollectionView?.collectionViewLayout as? SeaFoodLayout {
            layout.delegate = self
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deliveries.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DeliveryCell.identifier, for: indexPath) as! DeliveryCell
        cell.datasource = deliveries[indexPath.item]
        return cell
    }
    
}

extension CreateSeaFoodController{
    private func setupTextFieldDelegates(){
        self.foodNameField.delegate = self
        self.categoryField.delegate = self
        self.seaFoodAdressField.delegate = self
        self.seaFoodPriceField.delegate = self
        self.seaFoodDescriptionField.delegate = self
    }
}


extension CreateSeaFoodController{
    private func setupCompletedButtonViews(){
        self.completedButton.layer.masksToBounds = true
        self.completedButton.layer.cornerRadius = 8
    }
}


extension CreateSeaFoodController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    private func showImagePicker(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        self.present(imagePicker, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            self.images.value.append(selectedImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension CreateSeaFoodController{
    private func setupUploadImageButtonViews(){
        uploadImageButton.makeNoneBackgroundButton(_borderColor: #colorLiteral(red: 0.8156862855, green: 0.2039215714, blue: 0.2235294133, alpha: 1), _backgroundColor: UIColor.clear, _borderWidth: 0.5, _cornerRadius: 8)
    }
    
    //    picked_image_view_from_post_sea_food
    
    private func getImageFromImagePicker(){
        //        NotificationCenter.default.rx.notification(Notification.Name("post_selected_photo_lib")).takeUntil(self.rx.deallocated).subscribe { (notification) in
        //            if let selectedImage = notification.element?.userInfo?["image"] as? UIImage{
        //                self.images.value.append(selectedImage)
        //            }
        //   }
        
    }
}


extension CreateSeaFoodController: FSPagerViewDataSource, FSPagerViewDelegate{
    
    private func setupPagerView(){
        uploadedPhotosPagerView.delegate = self
        uploadedPhotosPagerView.dataSource = self
        uploadedPhotosPagerView.transformer = FSPagerViewTransformer(type: .overlap)
        let width: CGFloat = uploadedPhotosPagerView.frame.width - 56
        let height: CGFloat = uploadedPhotosPagerView.frame.height
        uploadedPhotosPagerView.itemSize = CGSize(width: width, height: height)
        
        // uploadedPhotosPagerView.backgroundView?.addSubview(UIImageView(image: #imageLiteral(resourceName: "04")))
        uploadedPhotosPagerView.register(CustomPhotoPagerCell.self, forCellWithReuseIdentifier: CustomPhotoPagerCell.identifier)
    }
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return  images.value.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: CustomPhotoPagerCell.identifier, at: index) as! CustomPhotoPagerCell
        cell.imageView?.image = images.value[index]
        return cell
    }
    
}

extension CreateSeaFoodController{
    private func getName() throws ->  String {
        if let name = foodNameField.text{
            if !name.isEmpty{
                return name
            }
        }
        throw XSeaError.UIError(XSeaErrorCode.IllegalSeaFoodName)
    }
    
    private func getCategoryId() throws -> String{
        
        if let selectedIndex = categoryField.selectedIndex{
            return self.categories[selectedIndex].id
        }
        throw XSeaError.UIError(XSeaErrorCode.IllegalSeaFoodCategory)
    }
    
    private func getPrice() throws -> Int{
        if let priceString = seaFoodPriceField.text{
            if !priceString.isEmpty {
                return Int(priceString)!
            }
        }
        
        throw XSeaError.UIError(XSeaErrorCode.IllegalSeaFoodPrice)
    }
    
    private func getDescription() throws -> String{
        if let description = seaFoodDescriptionField.text{
            if !description.isEmpty {
                return description
            }
        }
        
        throw XSeaError.UIError(XSeaErrorCode.IllegalSeaFoodDescription)
    }
    
    private func getSeaFoodAddress() throws -> String {
        if let address = seaFoodAdressField.text {
            if !address.isEmpty {
                return address
            }
        }
        
        throw XSeaError.UIError(XSeaErrorCode.IllegalSeaFoodAdress)
    }
}

extension CreateSeaFoodController{
    private func setupCategoryTextFieldData(){
        self.categoryField.optionArray = categories.map({ (category) -> String in
            category.name
        })
        self.categoryField.text = categories[0].name
        self.categoryField.selectedIndex = 0
    }
}

extension CreateSeaFoodController{
    private func fetchData(){
        self.categoryService.getCategories()
            .done {
                (categories) in
                self.categories = categories

            }.catch { (error) in
                
            }.finally {
        }
    }
}

extension CreateSeaFoodController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}



extension CreateSeaFoodController{
    
    private func getSeaFoodInfos() throws -> AddSeaFoodRequest{
        do{
            let name = try getName()
            let categoryId = try getCategoryId()
            //let address = try getSeaFoodAddress()
            let price = try getPrice()
            let description = try getDescription()
            
            
            let request = AddSeaFoodRequest(categoryId: categoryId, name: name, description: description, price: price, imageUrls: [])
            return request
        } catch XSeaError.UIError(let code){
            errorHandler.showUIError(code)
        } catch{
            errorHandler.showUIError(XSeaErrorCode.UnknowError)
        }
        
        throw XSeaError.UIError(XSeaErrorCode.UnknowError)
    }
}


extension CreateSeaFoodController: SeaFoodLayoutDelegate{
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 50
    }
}


