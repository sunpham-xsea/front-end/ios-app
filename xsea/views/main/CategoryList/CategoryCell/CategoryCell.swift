//
//  CategoryCell.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/31/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    var data: Category?{
        didSet{
            if let category = data{
                self.cateogoryName.text = category.name
            }
        }
    }
    var isFavorite: Bool = false{
        didSet{
            if self.isFavorite{
                favoriteButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                favoriteButton.alpha = 1.0
                favoriteButton.setImage(#imageLiteral(resourceName: "favorite_category"), for: .normal)
            } else{
                favoriteButton.backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.1294117719, blue: 0.1411764771, alpha: 1)
                favoriteButton.alpha = 0.5
                  favoriteButton.setImage(#imageLiteral(resourceName: "add"), for: .normal)
            }
        }
    }
    var viewColor :(from: UIColor, to: UIColor)?{
        didSet{
            if let color = viewColor{
            self.categoryInfoView.setVerticalDirectionGradientColor(from: color.from, to: color.to)
                self.categoryImageView.backgroundColor = color.to.toColor(color.from, percentage: 50)
            }
        }
    }
    public static let identifier = "CategoryCellId"
    public static let xibName = "CategoryCell"

    @IBOutlet weak var categoryImageView: UIView!
    @IBOutlet weak var categoryInfoView: UIView!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var cateogoryName: UILabel!
    

    @IBAction func favoriteButton_TouchUpInside(_ sender: UIButton) {
        self.isFavorite = !self.isFavorite
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupViews()
    }
}

extension CategoryCell{
    private func setupViews(){
        self.backgroundColor = UIColor.clear
        setupCategoryImageView()
        setupCategoryInfoView()
        setupFavoriteButtonView()
    }
}

extension CategoryCell{
    private func setupCategoryImageView(){
        self.categoryImageView.rouded()
        self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        categoryImage.rouded()
        
    }
    
    private func setupCategoryInfoView(){
        self.categoryInfoView.makeViewRound(cornerRadius: 8)
    }
}

extension CategoryCell{
    private func setupFavoriteButtonView(){
        self.favoriteButton.rouded()
        self.favoriteButton.imageEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
    }
}
