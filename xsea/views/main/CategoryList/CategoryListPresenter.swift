//
//  CategoryListPresenter.swift
//  xsea
//
//  Created Phạm Anh Tuấn on 8/31/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class CategoryListPresenter: CategoryListPresenterProtocol, CategoryListInteractorOutputProtocol {
    func showCategoryDetail(selectedIndex: Int) {
        router.pushToCategoryDetail(selecttionCategory: self.categories[selectedIndex], navColor: GradientColorList.colors[selectedIndex % GradientColorList.count].from)
    }
    
    var categories: [Category] = []
    
    func getCategories() {
        interactor?.getCategories()
    }
    
    func categoryListDidFetch(categories: [Category]) {
        self.categories = categories
        self.view?.showCategories()
    }
    

    weak private var view: CategoryListViewProtocol?
    var interactor: CategoryListInteractorInputProtocol?
    private let router: CategoryListWireframeProtocol

    init(interface: CategoryListViewProtocol, interactor: CategoryListInteractorInputProtocol?, router: CategoryListWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

}
