//
//  GradientColorList.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/31/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class GradientColorList{
    
    public static let colors:[(from: UIColor, to: UIColor)] = [
        (#colorLiteral(red: 1, green: 0.2799928955, blue: 0.4720628967, alpha: 1), #colorLiteral(red: 1, green: 0.5320161609, blue: 0.509626467, alpha: 1)),
        (#colorLiteral(red: 0.04679816607, green: 1, blue: 0.9717264346, alpha: 1), #colorLiteral(red: 0.5825435165, green: 1, blue: 0.8551599787, alpha: 1)),
        (#colorLiteral(red: 0.6123209236, green: 0.3391916796, blue: 1, alpha: 1), #colorLiteral(red: 0.8004210028, green: 0.5777994362, blue: 1, alpha: 1)),
        (#colorLiteral(red: 1, green: 0.6669073049, blue: 0.2191912072, alpha: 1), #colorLiteral(red: 1, green: 0.7673557313, blue: 0.4769936891, alpha: 1)),
        (#colorLiteral(red: 1, green: 0.2189569002, blue: 0.2682899381, alpha: 1), #colorLiteral(red: 1, green: 0.5688485799, blue: 0.5941347088, alpha: 1)),
        (#colorLiteral(red: 0.2497701365, green: 0.6443289687, blue: 1, alpha: 1), #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)),
        (#colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1), #colorLiteral(red: 1, green: 0.7577788303, blue: 0.4692645073, alpha: 1)),
        (#colorLiteral(red: 0.1979890849, green: 1, blue: 0.5961222231, alpha: 1), #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1))
    ]
    
    public static let count = GradientColorList.colors.count
}
