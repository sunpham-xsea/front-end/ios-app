//
//  SeaFoodCell.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/23/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class SeaFoodCell: BaseCollectionViewCell {
//
    public static let identifier = "SeaFoodCellId"
    public static let xibName =  "SeaFoodCell"
    
    @IBOutlet weak var seaFoodImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupViews()
    }
    
}

extension SeaFoodCell{
    private func setupViews(){
        self.backgroundColor = UIColor.black
    }
}

