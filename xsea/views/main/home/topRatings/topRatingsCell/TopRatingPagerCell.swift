//
//  TopRatingPagerCell.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit
import FSPagerView

class TopRatingPagerCell: FSPagerViewCell{
    
    public static let identifier = "TopRatingPagerCellId"
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {

    }
    
    private func setupView(){
        self.backgroundColor = UIColor.green
        self.imageView?.image = #imageLiteral(resourceName: "ratings_food_default")
    }
}
