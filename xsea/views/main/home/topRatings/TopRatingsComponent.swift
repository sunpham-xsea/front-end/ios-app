//
//  TopRatingsComponent.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit
import FSPagerView

class TopRatingsComponent: UICollectionReusableView, FSPagerViewDataSource, FSPagerViewDelegate{
    
    @IBOutlet weak var topRatingsPagerView: FSPagerView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    public static let indentifier = "TopRatingsComponentId"
    public static let xibName = "TopRatingsComponent"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupViews()
    }
    
    private func setupViews(){
        self.backgroundColor = UIColor.red

        setupTopRatingsPagerView()
    }
    
    private func setupTopRatingsPagerView(){
        self.topRatingsPagerView.delegate = self
        self.topRatingsPagerView.dataSource = self
        
        self.topRatingsPagerView.transformer = FSPagerViewTransformer(type: .depth)
        
        self.topRatingsPagerView.register(TopRatingPagerCell.self, forCellWithReuseIdentifier: TopRatingPagerCell.identifier)
    }
    
    private func setupPageControl(){
        self.pageControl.currentPage = 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: TopRatingPagerCell.identifier, at: index) as! TopRatingPagerCell
        return cell
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return 3
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        pageControl.currentPage = targetIndex
    }
    
    
}
