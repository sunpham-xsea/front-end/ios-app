//
//  UserFoodMenuBar.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/23/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class UserFoodMenuBar: UIView{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
}

extension UserFoodMenuBar{
    private func setupViews(){
        self.backgroundColor = UIColor.blue
    }
}

class UserFoodMenuCell: UIButton{
    
    override var isSelected: Bool{
        didSet{
            if self.isSelected == true {
                self.addBorder(side: .Bottom, color: .red, width: 4)
            }
            else{
                 self.addBorder(side: .Bottom, color: .white, width: 4.0)
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension UserFoodMenuCell{
   
}

