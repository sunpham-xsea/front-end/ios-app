//
//  ProfileInfoComponent.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/23/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class ProfileInfoComponent: UICollectionReusableView {

    public static let identifier = "ProfileInfoComponentId"
    public static let xibName = "ProfileInfoComponent"
    @IBOutlet weak var avatarImage: UIImageView!
    
    @IBAction func settingProfileButton_TouchUpInside(_ sender: Any) {
        print("Come to setting detail profile")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
        // Initialization code
    }
    
}

extension ProfileInfoComponent{
    private func setupViews(){
        avatarImage.rouded()
    }
}

extension ProfileInfoComponent{
    
}
