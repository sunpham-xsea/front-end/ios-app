//
//  UserProfileController.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/23/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class UserProfileController: UIViewController {
    
    
    @IBOutlet weak var postSeaFoodButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    @IBAction func postSeaFoodButton_TouchUpInside(_ sender: Any) {
        
        self.postSeaFoodButton.shake()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.performSegue(withIdentifier: "ProfileToPostSeaFood", sender: self)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension UserProfileController{
    private func setupsSeaFoodButton(){
        self.postSeaFoodButton.makeViewRound(cornerRadius: 8)
    }
}

extension UserProfileController{
    private func setupViews(){
        self.view.backgroundColor = UIColor.red
        self.navigationController?.navigationBar.isHidden = true
        setupCollectionViews()
        setupsSeaFoodButton()
    }
}

extension UserProfileController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    private func setupCollectionViews(){
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.contentInsetAdjustmentBehavior = .never
        
        collectionView.register(UINib(nibName: UserFoodsComponent.xibName, bundle: nil), forCellWithReuseIdentifier: UserFoodsComponent.identifer)
        
        collectionView.register(UINib(nibName: ProfileInfoComponent.xibName, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ProfileInfoComponent.identifier )
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ProfileInfoComponent.identifier, for: indexPath) as! ProfileInfoComponent
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 340)
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UserFoodsComponent.identifer, for: indexPath) as! UserFoodsComponent
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: self.view.frame.height - 340)
    }
    
    
}
