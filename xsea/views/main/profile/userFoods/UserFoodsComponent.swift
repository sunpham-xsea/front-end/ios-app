//
//  UserFoodsComponent.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/23/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit
import NotificationCenter

class UserFoodsComponent: UICollectionViewCell {
    
    private var seadFoodsDataSource:[String] = ["1234", "432424", "2", "3", "4", "5", "6"]
    
    @IBOutlet weak var collectionView: UICollectionView!
    public static let identifer = "UserFoodsComponentId"
    public static let xibName = "UserFoodsComponent"
    
    @IBOutlet weak var foodPostedButton: UserFoodMenuCell!
    @IBOutlet weak var foodOrderedButton: UserFoodMenuCell!
    
    @IBAction func foodOrderedButton_TuouchUpInside(_ sender: Any) {
        foodOrderedButton.isSelected = true
        foodPostedButton.isSelected = false
    }
    
    @IBAction func foodPostedButtonTouchUpInside(_ sender: Any) {
        foodOrderedButton.isSelected = false
        foodPostedButton.isSelected = true
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupViews()
    }
}

extension UserFoodsComponent{
    private func setupViews(){
        self.backgroundColor = UIColor.white
        setupUICollectionView()
        setupNotification()
        setupCollectionView()
        setupMenuBar()
    }
}

extension UserFoodsComponent{
    private func setupMenuBar(){
        foodPostedButton.isSelected = true
        foodOrderedButton.isSelected = false
    }
}

extension UserFoodsComponent{
    private func setupUICollectionView(){
        self.collectionView.backgroundColor = UIColor.white
        self.collectionView.showsHorizontalScrollIndicator = false
        
    }
}

extension UserFoodsComponent{
    private func setupNotification(){
        NotificationCenter.default.rx.notification(NotificationCustomName.AddSeaFoodSucceessFromPostSeaFoodCell).takeUntil(self.rx.deallocated)
            .subscribe { (notification) in
                if let seaFoodObj = notification.element?.userInfo?["sea_food_obj"] as? SeaFood{
                    print("seafood_obj_at_user_info_controller : \(seaFoodObj)")
                    self.seadFoodsDataSource.append(seaFoodObj.id)
                    self.collectionView.reloadData()
                }
        }
    }
}

extension UserFoodsComponent: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.seadFoodsDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BaseSeaFoodCell.identifer, for: indexPath) as! BaseSeaFoodCell
        return cell
    }
    
    private func setupCollectionView(){
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        //self.collectionView.contentInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        self.collectionView.register(UINib(nibName: BaseSeaFoodCell.xibName, bundle: nil), forCellWithReuseIdentifier: BaseSeaFoodCell.identifer)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 124)
    }
}
