//
//  Injectors.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation

class Injectors{
    private static var injector: Injector!
    
    static func initialize(modules: Module...){
        self.injector = Injector.builder()
            .withBinder(binder: Binder())
            .withModules(modules)
            .build()
    }
    
    static func get<T>(_ type: T.Type, name: String = "") -> T {
        guard let instance = injector.get(type, name: name) else {
            fatalError("\(type) has no instance provided")
        }
        return instance
    }
}
