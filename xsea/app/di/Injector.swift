//
//  Injector.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation
class Injector{
    private let binder: Binder
    
    init(binder: Binder) {
        self.binder = binder
    }
    
    static func builder()->InjectorBuilder{
        return InjectorBuilder()
    }
    
    func get<T>(_ type: T.Type, name: String = "") -> T?{
        return binder.get(type, name: name)
    }
    
    class InjectorBuilder{
        private var binder: Binder!
        private var modules: [Module] = []
        
        func withBinder(binder: Binder)-> InjectorBuilder{
            self.binder = binder
            return self
        }
        
        func withModules(_ modules:[Module])-> InjectorBuilder{
            self.modules = modules
            return self
        }
        
        func  build()-> Injector {

            modules.forEach { (module) in
                module.configure(binder: binder)
            }
            return Injector(binder: binder)
        }
    }
}
