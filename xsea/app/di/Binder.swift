//
//  Binder.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation


class Binding<T>{
    
    private let binder: Binder
    private let type: T.Type
    private var name:String = ""
    
    init(_ type: T.Type, binder: Binder) {
        self.binder = binder
        self.type = type
    }
    
    @discardableResult func to(instance any: Any)-> Binder{
        return binder.bind(type, instance: any, name: self.name)
    }
    
    func withName(name:String = "")-> Binding{
        self.name = name
        return self
    }
    
}

class Binder{
    
    private var map:[String: Any] = [:]
    
    @discardableResult func bind<T>(_ type: T.Type, instance any: Any, name: String = "") -> Binder{
        self.map[KeyIndentifier(type: type, name: name).mkKey()] = any
        return self
    }
    
    func bind<T>(_ type: T.Type)->Binding<T>{
        return Binding(type, binder: self)
    }
    
    func get<T>(_ type: T.Type, name: String = "")-> T{
        return (self.map[KeyIndentifier(type: type, name: name).mkKey()] as? T)!
    }
    
}

struct KeyIndentifier<T>{
    private let type: T.Type
    private let name: String
    
    init(type: T.Type, name: String = "") {
        self.type = type
        self.name = name
    }
    
    func mkKey()-> String{
        return "\(String(UInt(bitPattern: ObjectIdentifier(self.type))))-\(self.name)"
    }
}
