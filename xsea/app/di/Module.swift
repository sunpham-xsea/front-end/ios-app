//
//  Module.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation
protocol Module {
    func configure(binder: Binder)
}

class AbstractModule: Module{
    private var binder: Binder!
    
    func configure(){
        
    }
    
    func configure(binder: Binder) {
        self.binder = binder
        configure()
    }
    
    func bind<T>(type: T.Type)-> Binding<T>{
        return binder.bind(type)
    }
    
    public func get<T>(type: T.Type) -> T? {
        return binder?.get(type)
    }
    
}
