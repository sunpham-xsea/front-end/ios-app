//
//  DevModule.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation

class DevModule: AbstractModule{
    override func configure() {
        super.configure()
        bind(type: String.self)
            .withName(name: "api-host")
            .to(instance: "http://localhost:1997")
        
        bind(type: CategoryService.self).to(instance: CategoryServiceImpl())
        bind(type: ErrorHandler.self).to(instance: ErrorHandler())
        bind(type: SeaFoodService.self).to(instance: SeaFoodServiceImpl())
    }
}
