//
//  AppStoryBoard.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation
import UIKit

class AppStoryboard {
    
    private static func showStoryboard(identifier: String) {
//        xlog("Show storyboard \(identifier)")
        let storyboard = UIStoryboard(name: identifier, bundle: nil)
        let initial = storyboard.instantiateViewController(withIdentifier: identifier)
        UIApplication.shared.delegate?.window!?.rootViewController = initial
    }
    
    private static var isMainStoryboard: Bool = false
    
    static func showAuthStoryboard() {
        isMainStoryboard = false
        showStoryboard(identifier: "Auth")
    }
    
    static func showMainStoryboard() {
        isMainStoryboard = true
        showStoryboard(identifier: "Main")
    }
    
    static func isOnMain() -> Bool {
        return isMainStoryboard
    }
}


