//
//  ViewController.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit

class MainController: UIViewController {
    
    private let categoryService = Injectors.get(CategoryService.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryService.getCategories()
    }


}

