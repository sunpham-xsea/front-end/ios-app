//
//  JsonParser.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/23/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation

class JsonParser: NSObject {
    
    public static func decode<T: Decodable>(dataAsString data: String) -> T? {
        return decode(optData: data.data(using: .utf8))
    }
    
    public static func decode<T: Decodable>(optData: Data?) -> T? {
        if let data = optData {
            return decode(data)
        }
        return nil
    }
    
    public static func decode<T: Decodable>(_ data: Data) -> T? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        do {
            return try decoder.decode(T.self, from: data)
        } catch {
            print(error)
        }
        return nil
    }
    
    public static func encode<T: Encodable>(_ data: T) -> Data? {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        return try? encoder.encode(data)
    }
    
    public static func encode<T: Encodable>(_ data: T?) -> Data? {
        if let d = data {
            return encode(d)
        } else {
            return nil
        }
        
    }
}

