//
//  CategoryServiceImpl.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/21/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation
import PromiseKit

class CategoryServiceImpl: CategoryService{
    func getCategories() -> Promise<[Category]> {
        return HttpPromise.GET("/category/list")
    }
    
    
    
}
