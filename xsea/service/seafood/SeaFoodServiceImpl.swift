//
//  SeaFoodServiceImpl.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/25/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import Foundation
import PromiseKit

class SeaFoodServiceImpl: SeaFoodService{
    func createSeaFood(request: AddSeaFoodRequest) -> Promise<SeaFood> {
        return HttpPromise.POST("/seafood/add", body: request)
    }
    
}
