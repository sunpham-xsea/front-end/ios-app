//
//  Http.swift
//  xsea
//
//  Created by Phạm Anh Tuấn on 8/23/19.
//  Copyright © 2019 Phạm Anh Tuấn. All rights reserved.
//

import UIKit
import PromiseKit
import Alamofire

struct ObjectRequest: Codable {
    let objectType: String
    let objectIds: [String]
}

class XSeaReponse <T: Decodable>: Decodable {
    let success: Bool!
    let data: T?
    let connectionError: Error?
    
    init(success: Bool, data: T? = nil, connectionError: Error? = nil) {
        self.success = success
        self.data = data
        self.connectionError = connectionError
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.success = try values.decode(Bool.self, forKey: .success)
        self.data = try values.decode(T.self, forKey: .data)
        self.connectionError = nil
    }
    
    enum CodingKeys: String, CodingKey {
        case success, data
    }
}

typealias XSeaCompletionBlock<T: Decodable>  = (XSeaReponse<T>) -> Void


class HttpPromise{
    private static let queue = DispatchQueue(label: "HTTP queue", qos: .background, attributes: .concurrent, autoreleaseFrequency: .inherit, target: nil)
    
    private static func buildHeaders(inputHeaders: [String: String], credential: Bool = true) -> [String: String] {
        var headers = inputHeaders
        headers["Content-Type"] = "application/json"
        if (credential) {
            //MARK : do some thing in here for authorlization
        }
        return headers
    }
    
    
    
    static func request<T: Decodable>(
        _ url: String,
        method: String,
        body: Data? = nil,
        headers: [String: String] = [:],
        credential: Bool = true
        )  -> Promise<XSeaReponse<T>>{
        do{
            let fullUrl = "\(Injectors.get(String.self, name: "api-host"))\(url)"
            var request = try URLRequest(url: fullUrl,
                                         method: HTTPMethod(rawValue: method)!,
                                         headers: buildHeaders(inputHeaders: headers, credential: credential)
            )
            request.httpBody = body
            print("\(fullUrl)")
            
            return Alamofire.request(request).responseData(queue: queue)
                .map(on: queue) { data, response -> XSeaReponse<T> in
                    print(String(data: data, encoding: .utf8) ?? "nil")
                    if let json: XSeaReponse<T> = JsonParser.decode(data) {
                        if (json.success) {
                            return json
                        } else {
                            throw XSeaError.api_error
                        }
                    } else {
                        throw XSeaError.api_error
                    }
            }
            
        } catch{
            return Promise(error: error)
        }
    }
    public static func POSTData<T: Decodable>(_ url: String, body: Data? = nil, headers: [String: String] = [:], credential: Bool = true) -> Promise<T> {
        return request(url, method: "POST", body: body, credential: credential).map { (response: XSeaReponse<T>) -> T in
            guard let data = response.data else {
                throw XSeaError.APIError(APIErrorInfo.internal_error)
            }
            return data
        }
    }

    
    public static func GET<T: Decodable>(_ url: String) -> Promise<T>{
        return request(url, method: "GET").compactMap({ (response: XSeaReponse<T>) -> T in
            guard let data = response.data else{
                throw XSeaError.api_error
            }
            return data
        })
    }
    
    public static func POST<Req: Encodable, Resp: Decodable>(_ url: String,headers: [String: String] = [:], body: Req?,credential: Bool = true) -> Promise<Resp> {
        return POSTData(url,body: JsonParser.encode(body), headers: headers, credential: credential)
    }
}
